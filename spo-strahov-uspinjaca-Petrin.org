Uspinjača Petřín
vozi rutom Újezd ​​​​– Nebozízek - Petřín i povezuje se s tramvajskim linijama na stanici Újezd ​​​​.

Na samom vrhu prelazi preko Zida gladi.

Uspinjača s dva vagona doseže visinu veću od 130 m na stazi dugoj 510 m.

Uspinjača je izvorno bila na vodeni pogon kada je počela s radom 1891.; elektrificirana je 1932. godine.

Uspinjača je zbog velikih odrona 1965. godine obustavljena na 20 godina.


