PROMET
Javni prijevoz se sastoji od integrirane mreže praškog metroa, tramvajskog sustava, gradskih autobusa te žičare. 

Praški tramvajski sustav jedan je od većih u svijetu, s oko 900 kompozicija tramvajskih vozila, ukupne duljine tračnica od 518km (zg 148 km). Ima 33 linije, a godišnje preveze 356 milijuna putnika. Sve vrste prijevoza imaju zajednički sustav naplate. Javni prijevoz dodatno nadopunjuju taksi prijevoznici te turistički brodovi koji voze Vltavom. 

Osim toga, postoje i razne druge turističke vožnje, poput onih u automobilima oldtimerima ili kočijama u staroj jezgri grada. 

Prag je glavni čvor željeznica u Češkoj. Glavni željeznički kolodvor zove se Hlavní nádraží (Glavni kolodvor). Glavna praška zračna luka nosi naziv Praha Ruzyně te je jedna od najprometnijih zračnih luka u Srednjoj i Istočnoj Europi. Ostale zračne luke u gradu ili okolici su: Kbely (služi za domaće i međunarodne vojne snage, a ondje sa nalazi i Muzej češkog zrakoplovstva), zračna luka Letňany (služi za privatne zrakoplove) i zračna luka Aero Vodochody (koristi se kao pista za pokusne letove).

Glavni automobilski pravci prolaze centrom grada. Tunel Blanka predstavlja rješenje gradskih gužvi u sjeverozapadnom dijelu grada i sa svojih 5.5 km najduži je gradski tunel u Europi. Gradnja ovog objekta započela je 2007. Tunel je dio gradskog prstena cesta, koji služi kao prometno rješenje zbog velikog intenziteta prometa. Južni dio prstena otvoren je 22. rujna 2010. godine, dužine više od 20 km.

