Akademija Znanosti
Češku akademiju znanosti (skraćeno CAS, češki: Akademie věd České republiky, skraćeno AV ČR) osnovalo je 1992. godine Češko nacionalno vijeće kao češki nasljednik bivše Čehoslovačke akademije znanosti, a njezina tradicija seže još u Kraljevsku

Češko društvo znanosti (utemeljeno 1784.) i car Franjo Josip

Češka akademija znanosti, književnosti i umjetnosti (osnovana 1890.). Akademija je vodeća nesveučilišna javna istraživačka ustanova u Češkoj Republici.

Provodi i fundamentalna i strateška primijenjena istraživanja.

Ima tri znanstvena odjela, naime Odjel za matematiku, fiziku i znanosti o Zemlji, Odjel za kemijske i životne znanosti te Odjel za humanističke i društvene znanosti.

Akademija trenutno upravlja mrežom od šezdeset istraživačkih instituta i pet pratećih jedinica koje zapošljava ukupno 6400 zaposlenika, od kojih su više od polovice sveučilišni istraživači i doktori znanosti.

Glavni ured Akademije i četrdesetak istraživačkih instituta nalaze se u Pragu, dok su ostali instituti smješteni diljem zemlje.
