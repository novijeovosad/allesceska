Praska jubilarna izlozba - Prag 1891.
General Land Centennial Exhibition bila je svjetska izložba održana 1891. u Pragu, tadašnjoj Austro-Ugarskoj.

Mnoge su zgrade podignute za ovu izložbu, uključujući Industrijsku palaču i Křižíkovu svjetlosnu fontanu u Výstaviště Praha.

Sažetak
Održavajući se pred kraj Austro-Ugarskog carstva, ova je izložba bila demonstracija onoga što će uskoro postati želja Čehoslovačke za neovisnošću.

Njegov datum obilježava 100 godina od prve industrijske izložbe održane 1791. godine u praškom Clementinumu kada je Prag bio dio Habsburške Monarhije.

Njemačko stanovništvo u Pragu pokušalo je premjestiti izložbu iz 1891. za sljedeću godinu kada se nije mogla koristiti za obilježavanje stoljeća. A onda kada je održan uglavnom su ga bojkotirali.[2]

Ponekad poznat kao Praška jubilarna izložba, glavno mjesto sajma sada je Praški izložbeni prostor u blizini parka Stromovka.[2] Najveća zgrada bila je palača Průmyslový koju je projektirao Bedřich Münzberger

Otvor
Sajam je 15. svibnja 1891. otvorio nadvojvoda Karl Ludwig, a nazočili su mu ministri, guverner grof Franz Thun i princ George Lobkowicz.

Josef I. nije prisustvovao svečanom otvaranju, ali je sajam posjetio kasnije. Uvedena je i Křižíkova svjetlosna fontana.

Naslijeđe
Mnoge zgrade sa sajma još uvijek postoje, uključujući secesijski paviljon Hanava (blizu ogromnog metronoma) koji je popularan za vjenčanja i toranj Petřín, kopiju Eiffelovog tornja od 60 metara.

Křižíkova svjetlosna fontana nedavno je rekonstruirana i još uvijek radi.

