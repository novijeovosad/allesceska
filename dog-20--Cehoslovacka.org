Raspad Austrougarske
1918. raspala se Austro-Ugarska monarhija i Češka je zajedno sa Slovačkom stekla nezavisnost, ali se ujedinila u zajedničku državu Čehoslovačku, Prag je 28.10.1918. postao prijestonlica Čehoslovačke. 

1.1.1922. Prag je okupio 37 gradskih cjelina, čime je stvorena moderna metropola koja je 1938. godine imala blizu milijun stanovnika. 

Hitlerove trupe su okupirale grad 15.5.1939. Anti-fašistička borba je trajala više od 6 godina a obilježili su je zatvaranje češkog sveučilišta 1939. i ubojstvo “protektora” R. Hajdriha 1942. 

Vrhunac oslobodilačkih borbi je bio od 5.-9. svibnja 1945. 

Poslije izbora 1946. i Februarskih događanja 1948., na vlast je došla Komunistička partija, a u Čehoslovačkoj zavladao socijalizam. Poslije uspostavljanja programa “Put k socijalizmu”, došlo je do intenzivnog gospodarskog razvoja, u Pragu je izgrađeno 150 000 stanova, metro, mostovi i infrastruktura, ali je grad ipak počeo zaostajati za ostalim europskim metropolama. 

