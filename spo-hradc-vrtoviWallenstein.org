VRTOVI WALLENSTEIN 
podignut je 1630. i obuhvaća malo jezero, pećinu s umjetnim ukrasima i motivima iz grčke mitologije, fontanu, vrt s ružama, magnolijama i japanskim trešnjama. 

Pored vrta podignuta je i Valenštajnska palača u baroknom stilu, Viteška dvorana i astronomski opservatorij. 

Kipovi su kopije bronci iz 17. st. Originale su opljačkali Šveđani 1648. godine.

PALAČA WALLENSTEIN 
–  sagrađena je između 1624.-1630. za vojvodu Albrechta od Wallensteina, ova raskošna barokna palača trebala je nadmašiti Praški dvorac. 

Više od 20 kuća i gradska vrata srušeni su da bi se dobio prostor za palaču i vrt. Venerina fontana (1599.) nalazi se ispred lukova sale terrene. 

