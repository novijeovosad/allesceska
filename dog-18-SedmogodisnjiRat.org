Sedmogodisnji rat - Pruska
U 18. st. ponovo raste gospodarska snaga grada. 1757. je tijekom Sedmogodišnjeg rata pruska vojska zauzela i razorila grad. 1784. su pojedine gradske četvrti (4) Stari grad, Novi grad, Mali grad i Hradčani formalno ujedinjene (Josip II.) u jedan grad Prag, koji je postao centar gospodarskog razvoja i narodnog preporoda Češke. 

