* zrakoplovni muzej

Muzej je osnovan 1968. na području povijesnog vojnog aerodroma Prag-Kbely, prve zračne baze izgrađene nakon ustava Čehoslovačke 1918.

Zbog količine i kvalitete svojih zbirki jedan je od najvećih muzeja zrakoplovstva u Europi.

Trenutno zbirke sadrže 275 zrakoplova, od kojih je 85 izloženo u četiri zatvorene dvorane, 25 izloženo na otvorenom, 155 pohranjeno u depoima, a 10 zrakoplova, potpuno ispravnih, još uvijek je u službi.

Mnoge letjelice jedinstvene su u svijetu. (Muzejski prikaz samo jedne konstrukcije češkog aviona na svijetu i oba tipa Me 262 Schwalbe).

Izložba je usko povezana s poviješću čehoslovačkog i češkog zrakoplovstva, posebice njegove vojne grane.

Za usporedbu je izloženo i nekoliko drugih tipova važnih stranih zrakoplova, zajedno s brojnim zrakoplovnim motorima, dijelovima konstrukcija letjelica, naoružanjem, uniformama, zastavicama, znakovima razlikovanja i drugim relikvijama vezanim uz povijest čehoslovačkog i češkog zrakoplovstva.

Muzej je otvoren u ljetnoj sezoni, odnosno od svibnja do listopada, svakim danom osim ponedjeljka od 10.00 do 18.00 sati.

* zrakoplovni wik

Sredinom 1960-ih, Praški vojni muzej započeo je program za oporavak, restauraciju i očuvanje povijesnih zrakoplova iz cijele zemlje za eventualno izlaganje u Kbelyju.

U početku je korišten jedan hangar, a javno je izloženo pedesetak letjelica.

Zbirka zrakoplova nastavila je rasti, a jedan od izvornih Wagnerovih hangara na aerodromu stavljen je u upotrebu i sada sadrži izložene najstarije tipove zrakoplova.

Također je dodan još jedan hangar tipa Picha kako bi se broj hangara u kojima se nalazi velika i raznolika zbirka povećao na četiri.[1]

* zbirka 

Zbirka u Kbelyju sada uključuje 275 letjelica, od kojih je približno 110 izloženo javnosti u bilo kojem trenutku.[2]

Muzej sadrži mnoge češke dizajnirane i češke izgrađene zrakoplove koji datiraju od Prvog do Drugog svjetskog rata i nadzvučne mlazne lovce do 1960-ih.

Izloženo je nekoliko jedinstvenih tipova, uključujući Avia BH-11C L-BONK iz ranih 1920-ih.[3] Zrakoplovi iz Drugog svjetskog rata uključuju sovjetski Ilyushin Il-2 Shturmovik.

Ranije prikazani potpuno autentični Supermarine Spitfire LF.IX kojim je upravljala češka eskadrila Kraljevskog ratnog zrakoplovstva prebačen je svom vlasniku, Nacionalnom tehničkom muzeju u Pragu 2008. godine.

Osim brojnih izloženih vojnih zrakoplova i helikoptera, tu je i nekoliko putničkih zrakoplova sovjetskog dizajna, od kojih su neki prema licenci izgrađeni u Čehoslovačkoj.

Prikazani zrakoplovi uključuju Avia 14M (Ilyushin Il-14), Avia 14T, ex-CSA Ilyushin Il-18 i CSA Tupolev Tu-104.

Izloženi laki zrakoplovi uključuju češku Pragu E-114 Air Baby iz 1936., Mraz Sokol, Aero 45, Orlican L-40 Meta Sokol i Zlin 22 Junak. Izloženi helikopteri uključuju HC-2 Heli-Baby koji je dizajnirao VZLU (Institut za istraživanje i testiranje zrakoplovstva).[4]

Nakon 1. siječnja 1993., kada je Čehoslovačka podijeljena, nekoliko je zrakoplova iz zbirke prebačeno u Slovačku Republiku radi uključivanja u muzeje zrakoplovstva te zemlje.

Također, posljednjih godina muzej je zamijenio nekoliko zrakoplova češke i sovjetske konstrukcije za vojne zrakoplove iz SAD-a, UK-a, Švedske, Švicarske i drugih zemalja

