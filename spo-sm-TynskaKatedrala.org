TINSKI KVART 
- mjesto velike povijesne vrijednosti, blok građevina ulica Tinske, Stupartske i Male stupartske. 

Ovaj dio grada je izgrađen u 11. st. Ovdje su se nalazili trgovačka skladišta i imanja poznata kao “ungelti”, po kojima je cijeli kvart dobio ime. 

Najznačajnija građevina je Granovska palača, jedna od najbolje očuvanih renesansnih praških građevina. U periodu od 1984. do 1996. cijeli kvart je obnovljen. Danas on predstavlja kulturnu, trgovačku i administrativnu jezgru grada.

TINSKA CRKVA 

- poslije Katedrale Sv. Vida najznačajnija religiozna praška građevina u gotičkom stilu, s baroknim interijerom. 

Sagrađena je u sklopu nekadašnje bolnice tijekom 10. st., a u 14. st. je postala glavna crkva Starog grada. 

Crkva se sastoji od tri broda i s dvije strane je okružena kućama. Zbog Husitskih ratova završena je tek 1511. izgradnjom južnog crkvenog tornja. U crkvi se nalazi grob čuvenog danskog astronoma, Tiha Brahe, koji je radio na dvoru Rudolfa II. Dio interijera crkve je danas izmenjen. 

Onako kako katedrala sv. Vida dominira zapadnom obalom Vltave, tako je Týnský chrám najdojmljivija religijska građevina na istočnoj obali. 

Zanimljivo je da tornjevi na crkvi nisu simetrični, u skladu s tadašnjom praksom gotičke arhitekture, koja je trebala predstavljati i muški i ženski svijet. 

Týnský chrám je imao burnu povijest. Gradnja je započela u 14. stoljeću, no napredovala je vrlo sporo. Jedan od ključnih uzroka bili su reformacijski sukobi u Češkoj, koje su predvodili husiti i kojima je upravo ova crkva bila važno središte. 

Tako je primjerice južni toranj dovršen tek 1511. godine. No, i nakon dovršetka gradnje, obnavljana je više puta. Ključan razlog su bili požari, od kojih su dva najveća izbila 1679. i 1819. godine. 

Ulaz se nalazi u arkadama na Staromestskom trgu. Crkva je otvorena od utorka do subote u razdobljima od 10:00-13:00 i 15:00-17:00. Plaćanje ulaza nije obavezno, no obično se daje 20 kruna u svrhu obnove i održavanja građevine.
 
