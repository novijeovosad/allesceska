Mozart
U Pragu pronađeno glazbeno djelo koje su zajedno napisali Mozart i Salieri, a vjerovalo se da je izgubljeno 

Glazbeno djelo koje se smatralo izgubljenim, a zajednički su ga napisali austrijski skladatelj Wolfgang Amadeus Mozart i njegov talijanski kolega Antonio Salieri, pronađeno je u Pragu, objavljeno je u petak Mozart (1756.-1791.) je nekoliko puta boravio u Pragu, a 29listopada 1787. 

upravo je u tom gradu bila praizvedena njegova opera "Don Juan" za koju je libreto napisao Lorenzo Da Ponte (1749.-1838.), talijanski pjesnik i libretist. 

Glasine, koje svi stručnjaci odbacuju, da je Salieri (1750.-1825.) orkestrirao Mozartovu smrt jer mu je zavidio na genijalnosti, potječu iz priče "Mozart i Salieri" ruskog književnika Aleksandra Puškina. 

Tu je temu preuzeo engelski dramaturg Peter Shaffer za svoj komad "Amadeus" po kojem je češki redatelj Miloš Forman upravo u Pragu snimio svoj film "Amadeus" nagrađen s osam Oscara 1985. U 2016. obilježava se 260. rođendan Mozarta i 225. godišnjica njegove smrti.

Zanimljivo je dodati kako je Prag u razdoblju od 1990 do 2000bio vrlo popularno mjesto za snimanje filmova međunarodne produkcije te filmova iz Hollywooda i Bollywooda. Kombinacija arhitekture, niskih troškova te dobrih lokacija pokazala se privlačnom za filmsku industriju.

