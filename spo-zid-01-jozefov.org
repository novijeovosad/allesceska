ŽIDOVSKA ČETVRT - JOZEFOV
Naseljavanje još u 13. st; svojevremeno najveća židovska četvrt u Europi, 

Okupljali se sa Z oko staro nove sinagoge, s I oko Španjolske sinagoge; diskriminacija Lateranski koncil 13. st, kasnije posebna odjeća, odnos s kršćanima svodio se više-manje samo na trgovinu; 

odnos postao bolji dolaskom Josipa II. (sina Marije Terezije - Jozefov) na vlast koji im daje određena prava te su oni imali svoju vlastitu samoupravu; 

1 od najznačajnijih gradonačelnika bio je i Mordecai Marcus Meisel -  Meiselova sinagoga – jedna od najznačajnijih zbirki židovske umjetnosti na svijetu smještena je tu i u drugim zgradama Državnog židovskog muzeja. Postav uključuje sakralne predmete, namještaj i knjige.  

Stjecanjem prava Židovi iseljuju iz tog područja; 19. st. Sanacija područja zbog loših higijenskih uvjeta, veći dio i razrušen; ostalo 6 sinagoga i palača u art noveu stilu, 

1850. ovaj dio grada pripojen Pragu. U Pragu su nacisti htjeli otvoriti Muzej nestale rase (zato nisu uništili znamenitosti i sinagoge)

Josefov 
sam po sebi predstavlja obilježje praške židovske zajednice. Nastao je kao naselje židovskih trgovaca (Židovima u to vrijeme nije bio dozvoljen nikakav posao osim trgovine koja se smatrala nečasnim poslom za kršćane, tako su se izvještili u jedinom poslu kojeg su smjeli obavljati – trgovini) i dugo vremena je bio odvojen od ostalih dijelova grada zbog progona Židova koji su tijekom 18. st. činili četvrtinu praškog stanovništva. 

Očuvano je šest sinagoga: Klausova, Visoka, Pinkasova, Majselova, Stara-nova i Španjolska sinagoga. 

Glavna atrakcija trga je Židovski muzej, dragocjena baština sadašnje Češke. Muzej je jedan od najboljih na svijetu s jedinstvenom zbirkom židovske umjetnosti, tekstila i srebra koji podsjećaju na židovsku povijest. 

Židovska četvrt svoj je naziv dobila po caru Josipu II., čije su reforme omogućile bolje uvjete života za Židove. Danas je Židova svega 6 000. 

Dvije su slavne figure sinonim za ovaj dio grada. Franz Kafka koji je rođen 1883. godine u Pragu, te mitsko stvorenje Golem kojeg je stvorio Jehuda ben Bezalel poznatiji kao Rabi Low. 



* Ghetto, 
ljudi bili izolirani i tu živjeli skučeni, rodili se tu, umirali, pokapali, a nije bilo dovoljno prostora, u 12 razina pokapani, procjene da je na tom prostoru pokopano 12 000 ljudi, 

najpoznatiji grob Rabin Loew – golem (velik) – najščešće posjećen; običaj kad dođete na grob da stavite kamenčić

- Prag bio u centru njem. Reicha, mnoge stvari konzervirane, očuvane jer su nacisti tu htejli osnovati Muzej nestale rase, nekad živjeli, nikad više bili s nama (nekad protektor Henrich bio tu, najveći mrzitelj židova)

- sat, čitanje s desna na lijevo

