Vidikovac Petřín
jedna od najistaknutijih znamenitosti Praga

izgrađena je u sklopu Jubilarne izložbe 1891. kao labava kopija inspirirana Eiffelovim tornjem (u omjeru 1:5).

Visok je 63,5 metara, a do njegovog vrha, koji je na istoj nadmorskoj visini kao i pravi Eiffelov toranj, vodi 299 stepenica.

Pogled s njegovog vrha pruža ne samo cijeli grad, već se za vedrog dana može vidjeti gotovo cijela Bohemija.


