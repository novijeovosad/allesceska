Pivnice i pubovi
Češka je zemlja piva, a to zaista pokazuju velikom ponudom pivnica, gdje možete odlično jesti i piti uz vrlo nisku cijenu. 

Cijene piva u predgrađima počinju već od 20-ak kruna (oko 6 kuna), dok su u centru nešto više, ali svejedno ne bi trebale prelaziti 35 kruna za 0,5 l. Naravno, u 'turističkim' pivnicama su cijene više. 

Jedna od najpopularnijih i najstarijih pivnica je U Fleků/ Kod Fleka (podzemna Karlovo Náměstí, www.ufleku.cz), s pivovarom starom preko 500 godina! Ta pivovara proizvodi odlično crno pivo '13°', ali mu je mana velik broj turista, što dovodi do gužvi i viših cijena. Inače, u Fleku je osnovan HNK Hajduk.

„Kod vojaka Švejka“ – pivnica, s potpisima poznatih osoba na zidu iz gotovo cijelog svijeta. 

U Medvídků – pivovara s najjačim pivom u Češkoj - Tamni lager Xbeer 33. 

Novoměstský pivovar toči pivo s okusom karamela, kave, đumbira i banane. 

Pivovarski dom - pivski pjenušac Šamp pivovica, pivska votka ili pivski rum. 

Pivoteka BeerGeek na skladištu ima 500 vrsta flaširanog piva. 

Zlý časy – pivnica, ima tri šanka s nevjerojatnih 48 pivskih pipa. 
U Prvom pivskom tramvaju ustanovljena je opet tradicija takozvane četvrte pipe - uz tri stalna piva u ponudi je specijal, koji se stalno mijenja. ???

Specijalitet legendarne pivnice U Zlatého tygra, gdje je rado sjedio pisac Bohumil Hrabal, je pivski sir (podzemna Staroměstská, www.uzlatehotygra.cz), odlična pivnica s pravim praškim ugođajem. Zlatnom tigru došao je Bill Clinton, za posjeta češkom predsjedniku Vaclavu Havelu. 

Nešto mirnije i manje 'turističke' pivnice su npr. Pivovarský dům (www.gastroinfo.cz/pivodum), 

U Vejvodů (www.restauraceuvejvodu.cz) i 

U Medvídků (podzemna Národní třída). 

O'Che's (podzemna Staroměstská, (www.oches.com), jedinstvena kombinacija irskog i kubanskog puba!



* U Medvidku pivnica
– tradicionalna je češka pivnica ili bar, koja datira iz sredine 15. stoljeća, kada toči vlastito pivo. Pivnica "U Medvedik" je pravi češki bar koji može ugostiti nekoliko stotina ljudi, a nudi kako točeno pivo, pivo u bocama, tako i svježe proizvedeno pivo.

* U Fleku restoran / pivnica –
je najstariji pub u Pragu, restoran i pivovara spojeni u jednu namjenu koja predstavlja golemi pub sastavljen od nekoliko prostorija, čije je osoblje baš "štrebersko" kako bi se osjećali kao kod kuće.

U Fleku možete probati kakva je tradicionalna češka kuhinja, piva i uopšte provod u Češkoj.


* U Sedmi Svabu
srednjovjekovna pivnica – jedinstvena pivnica srednjovjekovnog izgleda s drvenim klupama, kamenim zidovima i velikim kaminom u čijim kutovima kao ukras stoje srednjovjekovni viteški oklopi. Svaki stol osvijetljen je svijećama.

Ima još puno pubova koje biste trebali posjetiti ako se odlučite putovati u Prag, a ako želite provesti odmor koji posjet Pragu, učenje o češkoj povijesti i otkrivanje kulture Praga može zahtijevati. Rezervirajte svoje mjesto za novogodišnje putovanje u Prag na vrijeme i provedite nezaboravan doček 2017. godine u Pragu, glavnom gradu Češke i prijestolnici piva, uz najbolje pivo na svijetu.



