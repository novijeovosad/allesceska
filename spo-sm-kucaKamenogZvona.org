KUĆA “KAMENOG ZVONA” (do crkve Tynske)
- neo-renesansna građevina sagrađena u periodu od 1876.-1884. s namjerom da tu bude galerija slika i zbirka antikviteta. 

U periodu od 1918.-1938. i od 1945.-1946. ovo je bila zgrada Narodne skupštine, a od 1946. Češkog filharmonijskog orkestra. 

Ovdje se nalazi i svjetski poznata Dvoržakova koncertna dvorana u kojoj se svake godine održava Praški proljetni glazbeni festival. 

Odmah do nalazi se Kinsky palača u rokoko stilu sa Nacionalnom galerijom grafike
 
