Pariska ulica - Pařížská ulica u Pragu
Užitak kupovine u luksuznim buticima svjetski poznatih brendova.

Elegantne trgovine vrhunskih čeških dizajnera i butici svjetski poznatih brendova, jedinstveni antikvarijati čarobne atmosfere, tradicionalno češko staklo, porculan i nakit stvaraju iz Praga idealnu metropolu za kupovinu vrhunske robe.

Ovi se proizvodi uglavnom mogu kupiti u blizini Starogradskog trga u povijesnom središtu glavnog grada.


