KULA STAROG GRADA  
prestavlja kapiju između Starog grada i Karlovog mosta. 

Jedna je od najljepših europskih kula u gotičkom stilu. Sadrži figure Karla IV. i Venceslava IV, kao i izrezbarene ambleme teritorija koje su pripadale Karlovom carstvu. 
