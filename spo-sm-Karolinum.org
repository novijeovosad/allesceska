KAROLINUM - prvobitna zgrada Karlovog univerziteta, osnovanog još 1348. 

Kapela u gotskom stilu iz 1370. je prepravljena 1718. Danas se ovde održavaju ceremonije dodjele diploma Karlovog sveučilišta.

* DEKANAT KARLOVOG UNIVERZITETA 
– nalazi se nasuprot kazalištu; i ostatak prvog gotičkog studentskog doma u kome se odvijala nastava nakon osnivanja Sveučilišta. 

Najstarije sveučilište Srednje i Istočne Europe osnovano je 7.4.1348. osnivačkim dokumentom s potpisom i pečatom Karla IV. 

* Kompleks Karlovog sveučilišta
KARLOVO SVEUČILIŠTE 
Po osnivanju Karlovog sveučilišta, 1348. Prag je postao jedan od glavnih sveučilišnih centara Europe.

U okviru Sveučilišta djelovala su 4 fakulteta: teološki, medicinski, pravni, artistički (slobodne umjetnosti).

Studij su financirali vladar i crkva. Danas najveći značaj imaju Ekonomski, Tehnički, Kemijski fakultet i Akademija umjetnosti, naročito Fakultet za filmsku i televizijsku režiju (FAMU).

Veliki broj čuvenih režisera (među njima i dosta Jugoslovena) studiralo je na ovom fakultetu. Najpoznatiji su Miloš Forman (dobitnik Oskara za filmove “Let iznad kukavičjeg gnijezda” i “Amadeus”) i Jirži Mencel (“Strogo praćeni vozovi”).

Na Karlovom sveučilištu su studirali i znanstvenik Johan Purkinje, Nikola Tesla, Albert Einstein, arheolog Bedžih Hrozni i mnogi drugi znameniti pojedinci iz svih krajeva.


