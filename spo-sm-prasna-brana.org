PRASKA BRANA - PRAŠNA BRANA/BARUTNA KULA 
– to ime nosi od prusko-austrijskih ratova jer je u njoj tada bilo skladište baruta – češki prach. 

Na istom mjestu bila su i prijašnja vrata koja su u grad vodila s istočne strane odakle je najprije stizalo sunce te je prvi naziv ovog ulaza bio „odrana brana“ od riječi od rana = od jutra. 

Vratima su u Prag dovoženi srebro iz rudnika u Kutnoj Hori što je omogućilo snažan gospodarski razvoj u 14. st. 

Današnja Prašna brana nastala je na mjestu nekadašnjeg ulaza, a kamen temeljac postavio je 1475. kralj Vladislav Jagelović. 

Graditelji su bili Vaclav iz Žlutica i Matej Rejsek iz Postojeva u Moravskoj. 

Brana je visoka 65 m, a do gornjeg kata vodi 186 kamenih stepenica. 

Prvi kat sačinjavaju kipovi čeških vladara s grbovima zemalja kojima su vladali. 

Kod Premysla Otokara II. nalaze se grbovi Češke, Austrije, Moravske i Štajerske, a kod kralja Karla IV. grbovi Svetog Rimskog Carstva Njemačkog Naroda te luksemburški, češki i ugarski grb. 

Prašna brana spojena je s Obecnim dumom.

- građevina u gotičkom stilu, podignuta 1475. kao dio vrata između Starog i Novog grada na mjestu stare uništene kapije, 

u vrijeme krunjenja kralja Vladislava II. Kasnije je spojena sa susjednom Kraljevskom palačom, a ime Barutna kula je dobila u 18. st. kada je korištena kao skladište za municiju. Prepravljena je 1886. godine od strane arhitekte Jozefa Mokera, kada joj je dodan krov u neo-gotičkom stilu.

Na ovom mjestu nalaze se vrata iz 11. st. kada su bila jedan od 13 ulaza u Stari grad. 

Imala su slabu obrambenu funkciju, namjera je bila da njezina bogata skulpturna dekoracija poveća ugled Kraljevske palače. 

Prašná brána, velebni toranj u srednjem je vijeku bio jedan od ulaza u grad, uklopljen u obrambene zidine oko Staroga grada. Nalazi se na trasi nekadašnjeg 'Kraljevskog puta', kojim su kraljevi putovali na krunjenje - kroz Prašnu bránu, preko Karlovog mosta do Katedrale u Hradčanima. 

Bio je to jedan od 13 ulaza u grad, no danas su ostali očuvani samo on i toranj uz Karlov most. 

Izgradnja je započela 1475. godine, ali je naglo prekinuta 8 godina kasnije, kad se kralj preselio iz Staroga grada u Praški dvorac. 

Konačno je dovršen tek krajem 16. stoljeća, a današnji oblik dobio je krajem 19. stoljeća, kada je obnovljen u neogotičkom stilu. Ime je toranj dobio po tome što se u njemu čuvao barut, dok se danas u njemu nalazi omanja galerija, koja s 44 metra visine pruža odličan pogled na grad. Inače, cijeli toranj visok je 65 metara.

Cijene ulaznica i radno vrijeme
Toranj je od travnja do rujna otvoren od 10:00 do 22:00, u listopadu i ožujku od 10:00 do 20:00, a ostatak godine od 10:00 do 18:00. Karte je moguće kupiti do 30 minuta prije zatvaranja. Cijena redovne ulaznica iznosi 75 čeških kruna, dok umirovljenici, djeca i studenti do 26 godina starosti plaćaju 55 kruna. Djeca do šest godina starosti i osobe s invaliditetom plaćaju 25 kruna.

