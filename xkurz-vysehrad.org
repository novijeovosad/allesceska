VIŠEHRAD 
- Drugi najbolji dvorac Praga. 

Dvorac Vyšehrad na čijem su groblju ukopani mnogi slavni Česi kao što su Antonín Dvořák i Bedřich Smetana. 

Sjedište dinastije Premisla iz 10. st. Princ Vratislav I je ovde krunjen kraljevskom krunom 1085. 

Najstarija građevina na ovom mjestu je rotonda Sv. Martina. Karlo IV je za vrijeme svoje vladavine rekonstruirao Višehradski zamak i sagradio utvrđenja. 

Poslije Husitske revolucije zamak je prepravljen u baroknom stilu. Pored Crkve Sv. Petra i Pavla tu su i crkvena rezidencija, zemunice, park i groblje po imenu “Sloven” na kome su sahranjeni mnogi znameniti Česi: 
naučnik Purkinje, kompozitori Dvoržak i Smetana i drugi. 

Sa južnog kraja višehradskih utvrđenja pruža se lep pogled na grad. 
LEGENDA: Za Višehrad se vezuju mnogi mitovi i legende. 
Jedna od legendi, koja nije u potpunosti u skladu s povijesnim činjenicama, govori o postanku imena Praga. 

Naime, Višehradom je za vreme dinastije Premisla vladala princeza Libuša. Kako je zemlja narastala, ukazala se potreba za novim gradom pa su podanici upitali princezu gdje da ga sagrade. 
Ona im je predložila: “Tamo gdje su četiri elementa u skladu - plodno tlo, zdrav zrak, dovoljno šume za vatru i čista voda.” 
Pošto su se princeza i princ popeli na najvišu točku Višehrada, princeza je pri zalasku sunca ispružila ruku prema istoku, u pravcu gustih šuma i rekla: 
“Vidim visok dvorac čija slava doseže do zvijezda. Ukoliko, narode, pođete tamo u gustoj šumi srešćete čoveka koji delje prag za svoju kuću. 
Na tom mjestu podignite dvorac i nazovite ga po pragu koji čovek delje - Prag!” 


* VYŠEGRAD 
– utvrda na Vyšegradu vjerojatno je izgrađena polovinom 9. st. a gradili su je Premyslovići. 

Tada već postoje Hradčany ali i prva utvrda ove kraljevske obitelji u Levom Hradecu. 

Legenda kaže da je ovo 1. sjedište obitelji Premysl i da je odavde libuša, kćer vojvode kroka krenula u polja tražiti supruga kako narodom ne bi vladala žena. 

Na polju je našla orača Premysla i oni su osnivači ova vladarske dinastije. Činjenice govore da je i ovdje bilo kraljevsko sjedište ali tek od 1085. kada se tu kralj, na kratko preselio iz Hradčana. 

U to vrijeme postavljena je i crkva svetih Petra i Pavla koja i danas dominira na ovoj stijeni iznad Vltave. 

Povijest lokacije vezana je uz mnoge bitke i obrambene bastione ali i mnoge sakralne objekte. Ostala je očuvana rotunda sv. Martina iz 11. st., a takav način izgradnje rotundi stigao je u češke zemlje iz Dalmacije. 

Uz crkvu, spomenik sv. Vaclavu, najznačajnije je i Groblje velikana – mnoge poznate osobe češkog naroda iz kulture i umjetsnoti. 

Svima njima posvećen je spomenik Slavin, a primjerice kod spomenika Bedricha Smetane svake godine 11.05. pietnim aktom započinje festival ozbiljne glazbe pod nazivom „Praško proljeće“ (Pražske jaro). 

Održava se u najakustičnijoj dvorani u Pragu, u Rudolfinumu. 


Vyšehrad –Nekoć zabranjena tvrđava, danas popularno mjesto vikendima za lokalne ljude. Posjetite najstariju gradsku Romanesque rotunda-u, St.Martin, crkvu St.Peter i Paul-a. Uživajte u mirnoj šetnji u vrtovima.

* PRAŠKE ROTONDE 
- značajne su tri rotonde u romanesknom stilu: Zlatna rotonda sa kraja 11. veka, rekonstruisana 1865., Rotonda Sv. 1Longina sa kraja 12. veka, rekonstruisna 1844. i 1934. i Rotonda Sv. Martina iz 11. veka, rekonstruisana 1878. od strane višehradskog sveštenstva.

* HRAM SV.PETRA I PAVLA… 
